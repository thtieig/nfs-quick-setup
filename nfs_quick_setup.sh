#!/bin/bash

#######################################################################
# This script install NFS client/server on a CentOS/RHEL7 machine
#
# It also remove firewalld and install iptables, setting up basic rules
# 
# The script is designed to run on a brand new server
#
#######################################################################


NFS_NETWORK="192.168.4.0/24"
NFS_SERVER_IP="192.168.4.1"
NFS_DOMAIN="nfsdomain.loc"
NFS_SHARE="/nfsshare"
NFS_MOUNT="/mnt/nfs"


# Set a user on server and client to make sure to have permissions
# and ownership consistency across the nodes

NFS_USER="apache"
NFS_GROUP="apache"
NFS_UID="48"
NFS_GID="48"
NFS_USER_DIR="www"

#######################################################################

install_nfs_client () {
yum install nfs-utils -y
}

install_nfs_server () {
yum install nfs-utils nfs-utils-lib -y
}

conf_nfs_server () {
mkdir -p $NFS_SHARE
echo "$NFS_SHARE $NFS_NETWORK(rw,sync,no_root_squash)" >> /etc/exports
systemctl enable nfs-server
systemctl start rpcbind
systemctl start nfs-server
}

set_nfs_user () {
groupadd -g $NFS_GID $NFS_GROUP
useradd -g $NFS_GID -u $NFS_UID $NFS_USER
}

set_nfs_user_dir () {
mkdir -p $NFS_SHARE/$NFS_USER_DIR
chown $NFS_UID:$NFS_GID $NFS_SHARE/$NFS_USER_DIR
chmod 755 $NFS_SHARE/$NFS_USER_DIR
}

install_firewall () {
yum install -y iptables-services
mv /etc/sysconfig/iptables{,.ORIG}

cat <<EOF > /etc/sysconfig/iptables
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT 
-A INPUT -p icmp -j ACCEPT 
-A INPUT -i lo -j ACCEPT 
-A INPUT -p tcp -m conntrack --ctstate NEW -m tcp --dport 22 -j ACCEPT 
#-A INPUT -p tcp -m conntrack --ctstate NEW -m tcp --dport 80 -j ACCEPT
#-A INPUT -p tcp -m conntrack --ctstate NEW -m tcp --dport 443 -j ACCEPT
-A INPUT -s  $NFS_NETWORK -m comment --comment "NFS Network" -j ACCEPT
-A INPUT -j REJECT --reject-with icmp-host-prohibited 
-A FORWARD -j REJECT --reject-with icmp-host-prohibited 
COMMIT
EOF

systemctl stop firewalld
systemctl mask firewalld
systemctl enable iptables
systemctl start iptables
}

set_nfs_domain () {
# Setting up the NFS Domain (this can help with file permissions)
# Does not need to match a real domain
sed -i "s/#Domain = local\.domain\.edu/Domain = $NFS_DOMAIN/" /etc/idmapd.conf
}

mount_nfs_share () {
# Hardly code nfsserver's IP into /etc/hosts to avoid DNS lookups
echo -e "\n##### NFS IP #####\n$NFS_SERVER_IP nfsserver\n###################" >> /etc/hosts
mkdir -p $NFS_MOUNT
mount -t nfs4 -o noatime,proto=tcp,actimeo=3,hard,intr,acl,_netdev nfsserver:$NFS_SHARE $NFS_MOUNT && tail -1 /proc/mounts >> /etc/fstab
}

all_done () {
clear
echo "Happy days! your NFS $1 has been setup!"
}


#######################################################################

case "$1" in
  client|CLIENT|Client)
	install_firewall
	install_nfs_client
	set_nfs_user	
	set_nfs_user_dir
	set_nfs_domain
	mount_nfs_share
	all_done
	;;

  server|SERVER|Server)
	install_firewall
	install_nfs_server
	set_nfs_user
	set_nfs_domain
	conf_nfs_server
	all_done
	;;

  *)
	echo "Use $0 server|client"
	;;
esac


